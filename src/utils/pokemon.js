class Pokemon {
    constructor(name = "Pika", age = 15) {
        this.name = name
        this.age = age
        this.strength = 1000
        this.life = 1000
        this.spells = [
            {
                'id': 0,
                'name': 'Kamehameha',
                'brutDamage': 10,
                'magicalDamage': 50
            },
            {
                'id': 1,
                'name': 'Adoken',
                'brutDamage': 100,
                'magicalDamage': 0
            },
            {
                'id': 2,
                'name': 'Slap',
                'brutDamage': 50,
                'magicalDamage': 0
            }
        ]
        this.armure = 10
        this.spellResistance = 20
    }

    // ACTIONS
    dealDamage(amountBrut = 0, amountMagical = 0) {
        this.life -= ((amountBrut - (amountBrut * (this.armure / 100)))) + ((amountMagical - (amountMagical * (this.spellResistance / 100))))
        this.life = this.life < 0 ? 0 : this.life

        return this.life
    }
}

export default Pokemon