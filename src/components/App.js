// Imports
import React from 'react'
import { connect } from 'react-redux'

import Pokemon from '@/utils/Pokemon'

import { startIncrementCount, startDecrementCount, startSetCount } from '../redux/actions/countActions'


// Component
class App extends React.Component {

    state = {
        pokemon: new Pokemon,
        enemy: new Pokemon,
        pokemonHaveDamage: false,
        enemyHaveDamage: false,
    }

    dealEnemyDamage = (brutDamage, magicalDamage) => {
        this.state.enemy.dealDamage(brutDamage, magicalDamage)
        this.setState({
            enemyHaveDamage: true
        })

        setTimeout(() => {
            this.setState({
                enemyHaveDamage: false
            })
            this.dealPokemonDamage(brutDamage, magicalDamage)
        }, 100)
    }

    dealPokemonDamage = (brutDamage, magicalDamage) => {
        this.state.pokemon.dealDamage(brutDamage, magicalDamage)
        this.setState({
            pokemonHaveDamage: true
        })

        setTimeout(() => {
            this.setState({
                pokemonHaveDamage: false
            })
        }, 100)
    }

    render() {
        return (
            <div className="fight__room">
                <div className="left">
                    <img className={this.state.pokemonHaveDamage ? 'anim' : ''} src="https://assets.pokemon.com/assets/cms2/img/pokedex/full/012.png" />
                    <p>{this.state.pokemon.life}</p>
                    {
                        this.state.pokemon.spells.map((spell) =>
                            <button
                                key={spell.id}
                                onClick={() => { this.dealEnemyDamage(spell.brutDamage, spell.magicalDamage) }}
                            >
                                {spell.name}
                            </button>
                        )
                    }
                </div>
                <div className="right">
                    <img className={this.state.enemyHaveDamage ? 'anim' : ''} src="https://assets.pokemon.com/assets/cms2/img/pokedex/full/003.png" />
                    <p>{this.state.enemy.life}</p>
                </div>
            </div>
        )
    }

}





// Export
const mapStateToProps = (state) => {
    return {
    }
}

export default connect(mapStateToProps)(App)