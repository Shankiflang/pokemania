import React from 'react'

export default (props) => {
    let type

    const handleClick = (brutDamage, magicalDamage) => {
        props.dealDamage(brutDamage, magicalDamage)
    }

    return (
        <div>
            <img className={props.getDamage ? 'anim' : 'bite'} src="https://assets.pokemon.com/assets/cms2/img/pokedex/full/003.png" />
            <p>{props.life}</p>
            {
                rops.spells.map((spell) =>
                    <button
                        key={spell.id}
                        onClick={() => { props.dealDamage(spell.brutDamage, spell.magicalDamage) }}
                    >
                        {spell.name}
                    </button>
                )
            }
        </div>
    )
}